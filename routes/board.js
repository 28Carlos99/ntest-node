const express = require('express');
const router = express.Router();
const { createBoard, updateBoardStage, getAllBoards } = require('../models/board');

// Ruta para crear un nuevo tablero
router.post('/', async (req, res) => {
  // Extraigo el título del tablero del cuerpo de la solicitud
  const { title } = req.body;
  try {
    // Intento crear un nuevo tablero utilizando el título proporcionado
    const board = await createBoard(title);
    // Envío una respuesta con el tablero creado y el código de estado 201 (creado)
    res.status(201).json(board);
  } catch (error) {
    // Si hay algún error, envío una respuesta con el código de estado 500 (error interno del servidor)
    res.status(500).send('Error al crear el tablero');
  }
});

// Ruta para actualizar la etapa de un tablero existente
router.put('/:id', async (req, res) => {
  // Extraigo el ID del tablero de los parámetros de la solicitud
  const { id } = req.params;
  // Extraigo la nueva etapa del tablero del cuerpo de la solicitud
  const { stage } = req.body;
  try {
    // Intento actualizar la etapa del tablero utilizando el ID y la nueva etapa proporcionados
    const updatedBoard = await updateBoardStage(parseInt(id), stage);
    // Verifico si se pudo encontrar y actualizar el tablero
    if (!updatedBoard) {
      // Si no se pudo encontrar el tablero, envío una respuesta con el código de estado 404 (no encontrado)
      res.status(404).send('Tablero no encontrado');
    } else {
      // Si se pudo actualizar el tablero, envío una respuesta con el tablero actualizado y el código de estado 200 (éxito)
      res.status(200).json(updatedBoard);
    }
  } catch (error) {
    // Si hay algún error, envío una respuesta con el código de estado 500 (error interno del servidor)
    res.status(500).send('Error al actualizar el tablero');
  }
});

// Ruta para obtener todos los tableros existentes
router.get('/', async (req, res) => {
  try {
    // Intento obtener todos los tableros existentes
    const boards = await getAllBoards();
    // Envío una respuesta con todos los tableros y el código de estado 200 (éxito)
    res.json(boards);
  } catch (error) {
    // Si hay algún error, envío una respuesta con el código de estado 500 (error interno del servidor)
    res.status(500).send('Error al obtener los tableros');
  }
});

module.exports = router;
