const db = require('./database');

// Ejecutar las consultas SQL para crear las tablas y definir el esquema
db.serialize(() => {
  db.run(`
    CREATE TABLE IF NOT EXISTS boards (
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      title TEXT NOT NULL,
      stage INTEGER NOT NULL
    )
  `);
});

// Cerrar la conexión con la base de datos después de ejecutar las consultas
db.close((err) => {
  if (err) {
    console.error('Error al cerrar la conexión con la base de datos', err.message);
  } else {
    console.log('Conexión con la base de datos cerrada correctamente');
  }
});
