// Importo Express y el middleware body-parser para manejar datos JSON
const express = require('express');
const bodyParser = require('body-parser');

// Importo el enrutador de tableros desde su archivo correspondiente
const boardRouter = require('./routes/board');

// Creo una instancia de la aplicación Express
const app = express();

// Defino el puerto en el que se ejecutará el servidor, utilizando el puerto proporcionado por el entorno o el puerto 3000 por defecto
const PORT = process.env.PORT || 3000;

// Configuro Express para que utilice body-parser para parsear datos JSON en las solicitudes
app.use(bodyParser.json());

// Establezco las rutas para los endpoints relacionados con los tableros, utilizando el enrutador de tableros
app.use('/boards', boardRouter);

// Hago que la aplicación Express escuche las solicitudes en el puerto especificado y muestre un mensaje cuando el servidor esté en funcionamiento
app.listen(PORT, () => {
  console.log(`Servidor corriendo en el puerto ${PORT}`);
});
