// Importa la conexión a la base de datos
const db = require('../database');

// Función para crear un nuevo tablero
function createBoard(title) {
  return new Promise((resolve, reject) => {
    db.run('INSERT INTO boards (title, stage) VALUES (?, ?)', [title, 1], function (err) {
      if (err) {
        reject(err);
      } else {
        resolve({ id: this.lastID, title, stage: 1 });
      }
    });
  });
}

// Función para actualizar la etapa de un tablero existente
function updateBoardStage(id, stage) {
  return new Promise((resolve, reject) => {
    db.run('UPDATE boards SET stage = ? WHERE id = ?', [stage, id], function (err) {
      if (err) {
        reject(err);
      } else if (this.changes === 0) {
        resolve(null); // No se encontró ningún tablero con el ID proporcionado
      } else {
        resolve({ id, stage });
      }
    });
  });
}

// Función para obtener todos los tableros existentes
function getAllBoards() {
  return new Promise((resolve, reject) => {
    db.all('SELECT * FROM boards', (err, rows) => {
      if (err) {
        reject(err);
      } else {
        resolve(rows);
      }
    });
  });
}

// Exporta las funciones del modelo para que puedan ser utilizadas en otros archivos
module.exports = {
  createBoard,
  updateBoardStage,
  getAllBoards
};
